#!/usr/bin/env julia

using Distributions
using Statistics
using Random
using Plots
theme(:dark)

# Types and contructors

mutable struct Agent
    pos::Vector{Int}
    starting_sugar::Float64
    sugar::Float64
    vision::Int
    metabolism::Float64
    age::Int
    life_span::Int
    alive::Bool
    income::Vector{Float64}
    fertile_range::Tuple{Int64, Int64}
    sex::Symbol
    id::Int
    children::Vector{Int}
end

struct Landscape
    level::Matrix{Float64}
    capacity::Matrix{Float64}
    grow::Matrix{Float64}
    occupied::BitMatrix
    # "The neighbor connection network is a directed graph with agents as the nodes and edges drawn to the agents who have been their neighbors[...]" (p. 39)
    network::Dict{Vector{Int}, Vector{Vector{Int}}}
    pollution::Matrix{Float64}
end

function Agent(pos::Vector{Int}, config::Dict{Symbol, Any}, sug::Union{Float64,Nothing}=nothing)
    if isnothing(sug)
        sugar = config[:agent_sugar]()
    else
        sugar = sug
    end
    sex = rand([:m,:f])
    Agent(
        pos,                                # position
        sugar,                              # starting_sugar
        sugar,                              # sugar
        config[:agent_vision](),            # vision
        config[:agent_metabolism](),        # metabolism
        config[:agent_age](),               # age
        config[:agent_life_span](),         # life_span
        true,                               # alive
        Float64[],                          # income
        config[:agent_fertile_range](sex),  # fertile_range
        sex,                                # sex
        rand(Int),                          # id
        Int[]                               # children
    )
end

function Agent(pos::Vector{Float64}, config::Dict{Symbol, Any}, sug::Union{Float64,Nothing}=nothing)
    Agent(round.(Int,pos), config, sug)
end

function Landscape(agents::Union{Vector{Agent},Nothing}=nothing, alt::Symbol=:rate; shape=(50,50), rate=1.0, positions::Union{Vector{Vector{Int}},Nothing}=nothing)
    landscape = nothing
    # "[Sugar] could grow back at a rate of one unit per time step[, or] at
    # different rates at different regions of the sugar scape." (p. 22)
    if alt == :random
        landscape = Landscape(
            zeros(shape),
            random_landscape(shape=shape),
            random_landscape(shape=shape),
            ones(shape) .!= ones(shape),
            Dict(),
            zeros((shape))
        )
    elseif alt == :rate
        landscape = Landscape(
            zeros(shape),
            random_landscape(shape=shape),
            ones(shape)*rate,
            ones(shape) .!= ones(shape),
            Dict(),
            zeros((shape))
        )
    elseif alt == :fixed
        if isnothing(positions)
            positions = [[round(Int, shape[1]/4), round(Int, 3*shape[2]/4)], [round(Int, 3*shape[1]/4), round(Int, shape[2]/4)]]
        end
        landscape = Landscape(
            zeros(shape),
            random_landscape(positions, shape=shape),
            ones(shape)*rate,
            ones(shape) .!= ones(shape),
            Dict(),
            zeros((shape))
        )
    end
    # "[...] the sugar level at every site initially at its capacity value." (p.21)
    grow!(landscape, :inf)
    if !isnothing(agents)
        occupy_landscape!(landscape, agents)
    end
    return landscape
end

# General

relu(x) = x < 0 ? 0.0 : x

running_mean(data, num=10) = [ mean([ data[d-i] for i in 0:min(d-1,num-1) ]) for d in 1:length(data) ]

# Distributions

function random_covariance_matrix(;covariance=(120,120))
    return ((rand()*covariance[1]/2)+covariance[1], (rand()*covariance[2]/2)+covariance[2]) |> x -> (x[1], x[2], x[1]*x[2]) |> x -> (x[1], x[2], sqrt(x[3] - rand()*x[3])) |> x -> [
        x[1] 0;
        0 x[2]
    ]
end

function random_normal(position::Union{Vector{Int},Nothing}=nothing;shape=(50,50),padding=(0,0),covariance=(120,120))
    if isnothing(position)
        return MvNormal(random_position(shape=shape,padding=padding), random_covariance_matrix(covariance=(covariance .* (shape ./ (50,50)).^2.5)))
    else
        return MvNormal(position, random_covariance_matrix(covariance=(covariance .* (shape ./ (50,50)).^1.5)))
    end
end

function random_pdf(positions::Union{Vector{Vector{Int}},Nothing}=nothing;shape=(50,50),padding=(-10,-10),signs=(10,2),covariance=(120,120))
    if isnothing(positions)
        mixmodel = MixtureModel([random_normal(shape=shape,padding=padding,covariance=covariance) for _ in 1:signs[1]])
        mixmodel_neg = MixtureModel([random_normal(shape=shape,padding=padding,covariance=covariance) for _ in 1:signs[2]])
        return x -> pdf(mixmodel, x) - pdf(mixmodel_neg, x)
    else
        return x -> pdf(MixtureModel(random_normal.(positions;shape=shape,padding=padding,covariance=covariance)), x)
    end
end

# Landscape

## Functions

function cap_landscape!(level, capacity)
    level[:] = reshape([ level[i] > capacity[i] ? capacity[i] : level[i] for i in 1:length(level)], size(level))
end

function cap_landscape!(landscape::Landscape)
    cap_landscape!(landscape.level, landscape.capacity)
end

function occupy_landscape!(landscape::Landscape, agents::Vector{Agent})
    agents[:] = [
        begin
            while landscape.occupied[(agent.pos + [1,1])...] == true
                agent = perturb_agent_positions(agent; shape=size(landscape.level))
            end
            landscape.occupied[(agent.pos + [1,1])...] = true
            agent
        end
    for agent in agents ]
end

function random_position(;shape=(50,50),padding=(0,0))
    [rand()*(2shape[1])-padding[1], rand()*(2shape[2])-padding[2]]
end

function random_landscape(positions::Union{Vector{Vector{Int}},Nothing}=nothing;shape=(50,50),padding=(-10,-10),signs=(10,2),covariance=(30,30))
    # "The sugar [capacity] range from some maximum -- here 4 -- at the peaks to zero [...]." (p. 21-22)
    return map(random_pdf(positions; shape=shape,padding=padding,signs=signs,covariance=covariance), [[x,y] for x in 0:(shape[1]-1), y in 0:(shape[2]-1)]) |> x -> x/max(x...)*4 |> x -> relu.(x)
end

### Growing

function grow!(landscape::Landscape, rule::Symbol=:step, age::Int=0)
    if rule == :inf
        # "Various rules are possible. For instance, sugar could grow back instantly ot its capacity." (p. 22)
        landscape.level[:] = copy(landscape.capacity)
    elseif rule == :step
        # "Or it could grow back at a rate of one unit per time step. Or it could grow back at different
        # rates at different regions of the sugar scape." (p. 22)
        landscape.level[:] = landscape.level + landscape.grow
    elseif rule == :nnm
        # "Or the growback rate might be made to depend on the sugar level neighboring sites." (p. 22)
        landscape.level[:] = landscape.level + [
            0.4*mean([ landscape.level[([x,y] + v)...] for v in [[1,0], [-1,0], [0,1], [0,-1]] if (([x,y] + v) |> z -> 1 ≤ z[1] ≤ size(landscape.level)[1] && 1 ≤ z[2] ≤ size(landscape.level)[2]) ])
        for x in 1:size(landscape.level)[1], y in 1:size(landscape.level)[2]]
    elseif rule == :ns
        # "Sugarscape seasonal Growback rule" (p. 44)
        landscape.level[:] = landscape.level + relu.(((age % (2*50))/50 < 1 ? 1 : -1)*([ones((round(Int, size(landscape.level)[2]/2, RoundDown) + (isodd(size(landscape.level)[2]) ? 1 : 0), size(landscape.level)[1])) .- 1/8; -ones((round(Int, size(landscape.level)[2]/2, RoundDown), size(landscape.level)[1]))]')) .+ 1/8
    end
    cap_landscape!(landscape)
end

# Agents

## Functions

function random_agents(config::Dict{Symbol, Any}, center::Union{Vector{Int},Nothing}=nothing; shape=(50,50),length=250)
    if isnothing(center)
        return Agent[ Agent([rand()*(shape[1])-0.5, rand()*(shape[2])-0.5], config) for _ in 1:length ]
    else
        return Agent[ Agent(center, config) for _ in 1:length ]
    end
end

function move!(agents::Vector{Agent}, landscape::Union{Landscape,Nothing}=nothing, alt::Symbol=:look; size=1, shape=(50,50))
    actual_shape = shape
    if !isnothing(landscape)
        actual_shape = Base.size(landscape.level)
    end
    if alt == :random
        perturb_agent_positions!(agents; size=size, shape=actual_shape)
    elseif alt == :look
        look_and_move!(agents, landscape; shape=actual_shape)
    end
end

function perturb_agent_positions!(agents::Vector{Agent}; size=1, shape=(50,50))
    agents[:] = perturb_agent_positions.(agents; size=size, shape=shape)
end

function perturb_agent_positions(agent::Agent; size=1, shape=(50,50))
    #return Agent((agent.pos + [rand(-agent.vision:agent.vision)*size, rand(-agent.vision:agent.vision)*size]) % shape, getfield.(agent, filter(x -> x != :pos, fieldnames(typeof(agent))))...)
    return Agent((agent.pos + [rand(-1:1)*size, rand(-1:1)*size]) % shape, getfield.(agent, filter(x -> x != :pos, fieldnames(typeof(agent))))...)
end

function look_and_move!(agents::Vector{Agent}, landscape::Landscape; shape=(50,50))
    look_and_move!.(agents, landscape::Landscape; shape=shape)
end

function look_and_move!(agent::Agent, landscape::Landscape; shape=(50,50))
    best_pos = [agent.pos, landscape.level[(agent.pos+[1,1])...]/(landscape.pollution[(agent.pos+[1,1])...] + 1), 0]
    for i in -agent.vision:agent.vision
        current_pos = (agent.pos + [0,i]) % shape
        if (landscape.level[(current_pos+[1,1])...]/(landscape.pollution[(agent.pos+[1,1])...] + 1) > best_pos[2] || (landscape.level[(current_pos+[1,1])...]/(landscape.pollution[(agent.pos+[1,1])...] + 1) == best_pos[2] && best_pos[3] > abs(i))) && !landscape.occupied[(current_pos+[1,1])...]
            best_pos[:] = [current_pos, landscape.level[(current_pos+[1,1])...], i]
        end
        current_pos = (agent.pos + [i,0]) % shape
        if (landscape.level[(current_pos+[1,1])...]/(landscape.pollution[(agent.pos+[1,1])...] + 1) > best_pos[2] || (landscape.level[(current_pos+[1,1])...]/(landscape.pollution[(agent.pos+[1,1])...] + 1) == best_pos[2] && best_pos[3] > abs(i))) && !landscape.occupied[(current_pos+[1,1])...]
            best_pos[:] = [current_pos, landscape.level[(current_pos+[1,1])...], i]
        end
    end
    if best_pos[2] == 0.0
        pos = [
            [ (agent.pos + [0,i]) % shape for i in -agent.vision:agent.vision if !landscape.occupied[(((agent.pos + [0,i]) % shape)+[1,1])...] ];
            [ (agent.pos + [i,0]) % shape for i in -agent.vision:agent.vision if !landscape.occupied[(((agent.pos + [i,0]) % shape)+[1,1])...] ]
        ]
        if length(pos) > 0
            best_pos[:] = [ rand(pos), 0.0, 0 ]
        end
    end
    for vs in values(landscape.network)
        i = findfirst(x -> x == agent.pos, vs)
        if !isnothing(i)
            vs[i][:] = best_pos[1]
        end
    end
    landscape.network[best_pos[1]] = [ (agent.pos + v) % shape for v in [[1,0], [-1,0], [0,1], [0,-1]] if landscape.occupied[(((agent.pos + v) % shape) + [1,1])...] ]
    landscape.occupied[(agent.pos+[1,1])...] = false
    landscape.occupied[(best_pos[1]+[1,1])...] = true
    agent.pos[:] = best_pos[1]
end

function consume!(agents::Vector{Agent}, landscape::Landscape, age::Int, config::Dict{Symbol,Any})
    for agent in agents
        consume!(agent, landscape, age, config)
    end
end

function consume!(agent::Agent, landscape::Landscape, age::Int, config::Dict{Symbol,Any})
    if age ≥ config[:pollution_start]
        landscape.pollution[(agent.pos+[1,1])...] = landscape.pollution[(agent.pos+[1,1])...] + config[:consumption_pollution_coeff]*min(agent.metabolism, agent.sugar)
    end
    agent.sugar = agent.sugar - agent.metabolism
    # "If at any time the agent's sugar wealth falls to zero or below [...] it is removed from the sugarscape." (p. 25)
    if agent.sugar < 0
        agent.alive = false
    end
end

function isalive(agent::Agent, landscape::Union{Landscape,Nothing}=nothing)
    if !isnothing(landscape)
        if !agent.alive
            landscape.occupied[(agent.pos + [1,1])...] = false
            if haskey(landscape.network, agent.pos)
                delete!(landscape.network, agent.pos)
            end
            for (k,v) in landscape.network
                if agent.pos in v
                    filter!(x -> x != agent.pos, v)
                end
            end
        end
    end
    return agent.alive
end

function kill!(agents::Vector{Agent}, landscape::Landscape)
    filter!(x -> isalive(x, landscape), agents)
    for agent in agents
        agent.children = [ child_id for child_id in agent.children if child_id ∈ [ a.id for a in agents ] ]
    end
end

function repopulate!(agents::Vector{Agent}, landscape::Landscape, config::Dict{Symbol, Any})
    # "When an agent dies it is replaced by an agent of age 0 having random
    # genetic attributes, random position, random initial endowment, and a 
    # maximum age randomly selected from the range [a,b]" (p. 33)
    new_agents = random_agents(config; shape=size(landscape.level),length=(config[:agents]-length(agents)))
    occupy_landscape!(landscape, new_agents)
    append!(agents, new_agents)
end

function age!(agent::Agent)
    agent.age += 1
    if agent.age > agent.life_span
        agent.alive = false
    end
end

function spawn_child(parent1::Agent, parent2::Agent, pos::Vector{Int})
    return Agent(
        pos,
        mean([parent1.starting_sugar, parent2.starting_sugar]),
        mean([parent1.sugar, parent2.sugar]),
        rand([parent1.vision, parent2.vision]),
        rand([parent1.metabolism, parent2.metabolism]),
        0,
        (parent1.life_span, parent2.life_span) |> x -> rand(min(x...):max(x...)),
        true,
        Float64[],
        ((parent1.fertile_range[1], parent2.fertile_range[1]) |> x -> rand(min(x...):max(x...)), (parent1.fertile_range[2], parent2.fertile_range[2]) |> x -> rand(min(x...):max(x...))),
        rand([:m,:f]),
        rand(Int),
        Int[]
    )
end

function procreate!(agents::Vector{Agent}, landscape::Landscape, config::Dict{Symbol, Any})
    new_agents = Agent[]
    for agent in agents
        if haskey(landscape.network, agent.pos)
            if agent.fertile_range[1] ≤ agent.age ≤ agent.fertile_range[2] && length(landscape.network[agent.pos]) > 0 && agent.sugar ≥ agent.starting_sugar
                for u in landscape.network[agent.pos]
                    i = findfirst(a -> a.pos == u, agents)
                    if agents[i].fertile_range[1] ≤ agents[i].age ≤ agents[i].fertile_range[2] && agents[i].sex != agent.sex && agents[i].sugar ≥ agents[i].starting_sugar
                        empty_pos = [ ((agents[i].pos + v) % config[:shape]) for v in [[1,0], [-1,0], [0,1], [0,-1]] if !landscape.occupied[(((agents[i].pos + v) % config[:shape]) + [1,1])...] ]
                        if length(empty_pos) == 0
                            empty_pos = [ ((agent.pos + v) % config[:shape]) for v in [[1,0], [-1,0], [0,1], [0,-1]] if !landscape.occupied[(((agent.pos + v) % config[:shape]) + [1,1])...] ]
                        end
                        if length(empty_pos) > 0 && rand() < 0.3
                            pos = rand(empty_pos)
                            append!(new_agents, [spawn_child(agent, agents[i], pos)])
                            id = new_agents[end].id
                            agent.sugar = agent.sugar/2
                            append!(agent.children, id)
                            agents[i].sugar = agents[i].sugar/2
                            append!(agents[i].children, id)
                        end
                    end
                end
            end
        end
    end
    if length(new_agents) > 0
        append!(agents, new_agents)
    end
end

function inherit!(agents::Vector{Agent})
    for agent in agents
        if !agent.alive
            for child_id in agent.children
                i = findfirst(agent -> agent.id == child_id, agents)
                if !isnothing(i)
                    agents[i].sugar += agent.sugar/length(agent.children)
                end
            end
        end
    end
end

function update!(agents::Vector{Agent}, landscape::Landscape, config::Dict{Symbol, Any})
    inherit!(agents)
    kill!(agents, landscape)
    procreate!(agents, landscape, config)
    #repopulate!(agents, landscape, config)
    shuffle!(agents)
end

# World

function harvest!(landscape::Landscape, agents::Vector{Agent}, age::Int, config::Dict{Symbol, Any})
    for agent in agents
        harvest!(landscape, agent, age, config)
    end
end

function harvest!(landscape::Landscape, agent::Agent, age::Int, config::Dict{Symbol, Any})
    sugar = agent.sugar + landscape.level[(agent.pos+[1,1])...]
    if length(agent.income) == 10
        agent.income[:] = [agent.income[2:end]; [landscape.level[(agent.pos+[1,1])...]]]
    else
        append!(agent.income, [landscape.level[(agent.pos+[1,1])...]])
    end
    if age ≥ config[:pollution_start]
        landscape.pollution[(agent.pos+[1,1])...] = landscape.pollution[(agent.pos+[1,1])...] + config[:harvest_pollution_coeff]*landscape.level[(agent.pos+[1,1])...]
    end
    landscape.level[(agent.pos+[1,1])...] = 0.0
    agent.sugar = sugar
end

function diffuse_pollusion!(landscape::Landscape, age::Int, config::Dict{Symbol, Any})
    if age % config[:diffusion_interval] == 0 && config[:pollution_diffusion_start] ≤ age
        landscape.pollution[:] = [
            mean([ landscape.pollution[([x,y] + v)...] for v in [[0,0], [1,0], [-1,0], [0,1], [0,-1]] if (([x,y] + v) |> z -> 1 ≤ z[1] ≤ size(landscape.pollution)[1] && 1 ≤ z[2] ≤ size(landscape.pollution)[2]) ])
        for x in 1:size(landscape.pollution)[1], y in 1:size(landscape.pollution)[2]]
    end
end

# Method defintions for imported functions

## Base

import Base.rem

function Base.rem(vec::Vector{Int64}, tup::Tuple{Int64, Int64})
    [rem(vec[i], tup[i], RoundDown) for i in 1:length(vec)]
end

function Base.rem(vec::Vector{Float64}, tup::Tuple{Int64, Int64})
    [rem(round(vec[i]), tup[i], RoundDown) for i in 1:length(vec)]
end

import Base.broadcastable

function Base.broadcastable(landscape::Landscape)
    return Ref(landscape)
end

function Base.broadcastable(agent::Agent)
    return Ref(agent)
end

# Other

function gini(data)
    # https://en.wikipedia.org/wiki/Gini_coefficient#Alternative_expressions
    if length(data) == 0
        return 0
    end
    return sort(data) |> ys -> 2*sum(ys[i]*i for i in 1:length(ys))/(length(data)*sum(ys[i] for i in 1:length(ys))) - (length(data)+1)/length(data)
end

function lorenz(data)
    # https://en.wikipedia.org/wiki/Lorenz_curve#Definition_and_calculation
    n = length(data)
    if n == 0
        return ([],[])
    end
    x = [ i/n for i in 1:n ]
    y = sort(data) |> sdata -> [ sum( sdata[j]/n for j in 1:i ) for i in 1:n ] / sum( sdata[i]/n for i in 1:n )
    return (x, y)
end

# MAIN

function world_tick!(landscape::Landscape, agents::Vector{Agent}, age::Int, config::Dict{Symbol,Any}, history::Dict{Symbol,Vector{Float64}})
    grow!(landscape, config[:regrowth_rule], age)
    diffuse_pollusion!(landscape, age, config)
    move!(agents, landscape)
    harvest!(landscape, agents, age, config)
    alive = length(agents)
    consume!(agents, landscape, age, config)
    alive1 = length([ 0 for agent in agents if agent.alive ])
    age!.(agents)
    alive2 = length([ 0 for agent in agents if agent.alive ])
    #history[:pop_starve] = [history[:pop_starve]; [(length(history[:pop_starve]) > 0 ? history[:pop_starve][end] : 0) + alive - alive1]]
    #history[:pop_age] = [history[:pop_age]; [(length(history[:pop_age]) > 0 ? history[:pop_age][end] : 0) + alive1 - alive2]]
    history[:pop_starve] = [history[:pop_starve]; [alive - alive1]]
    history[:pop_age] = [history[:pop_age]; [alive1 - alive2]]
    update!(agents, landscape, config)
end

function update_history!(history, agents)
    history[:sugar_mean] = [history[:sugar_mean]; [mean([ agent.sugar for agent in agents ])]]
    history[:sugar_median] = [history[:sugar_median]; [median([ agent.sugar for agent in agents ])]]
    history[:sugar_min] = [history[:sugar_min]; [min([ agent.sugar for agent in agents ]...)]]
    history[:sugar_max] = [history[:sugar_max]; [max([ agent.sugar for agent in agents ]...)]]

    history[:wealth_gini] = [history[:wealth_gini]; [gini([ agent.sugar for agent in agents ])]]
    history[:income_gini] = [history[:income_gini]; [gini(mean.([ agent.income for agent in agents if length(agent.income) > 0 ]))]]

    history[:lives] = [history[:lives]; [length(agents)]]

    history[:age_mean] = [history[:age_mean]; [mean([ agent.age for agent in agents ])]]
    history[:age_median] = [history[:age_median]; [median([ agent.age for agent in agents ])]]
    history[:age_min] = [history[:age_min]; [min([ agent.age for agent in agents ]...)]]
    history[:age_max] = [history[:age_max]; [max([ agent.age for agent in agents ]...)]]

    history[:vision_mean] = [history[:vision_mean]; [mean([ agent.vision for agent in agents ])]]
    history[:vision_median] = [history[:vision_median]; [median([ agent.vision for agent in agents ])]]
    history[:vision_min] = [history[:vision_min]; [min([ agent.vision for agent in agents ]...)]]
    history[:vision_max] = [history[:vision_max]; [max([ agent.vision for agent in agents ]...)]]

    history[:metabolism_mean] = [history[:metabolism_mean]; [mean([ agent.metabolism for agent in agents ])]]
    history[:metabolism_median] = [history[:metabolism_median]; [median([ agent.metabolism for agent in agents ])]]
    history[:metabolism_min] = [history[:metabolism_min]; [min([ agent.metabolism for agent in agents ]...)]]
    history[:metabolism_max] = [history[:metabolism_max]; [max([ agent.metabolism for agent in agents ]...)]]

    history[:population] = [history[:population]; [length(agents)]]

    history[:fertility_age_ratio] = [history[:fertility_age_ratio]; [length([ agent for agent in agents if agent.fertile_range[1] ≤ agent.age ≤ agent.fertile_range[2]])/length(agents)]]
    history[:fertility_sugar_ratio] = [history[:fertility_sugar_ratio]; [length([ agent for agent in agents if agent.sugar > agent.starting_sugar])/length(agents)]]
end

function plot_world(landscape::Landscape, agents::Vector{Agent}, history::Dict, xs, ys, age, limit=:all)
    lw = 3

    xr = history[:sugar_mean] |> l -> 1:length(l)
    if typeof(limit) <: Int
        xr = history[:sugar_mean] |> l -> max(1,length(l)-limit):length(l)
    end

    # World
    world_plot = heatmap(xs, ys, transpose(landscape.level), legend=false, ticks=false, clims=(0,max(landscape.capacity...)))
    heatmap!(xs, ys, transpose(landscape.pollution), c=cgrad([:black, :green], alpha=0.4))
    scatter!(([agent.pos[1] for agent in agents], [agent.pos[2] for agent in agents])..., markersize=5, label="", color=:white)
    for (k,vs) in landscape.network
        x = [[k[1]]; [ v[1] for v in vs]]
        y = [[k[2]]; [ v[2] for v in vs]]
        plot!(x,y, linewidth=3, color=:blue)
    end
    for agent in agents
        x = [[agent.pos[1]]; [ a.pos[1] for a in agents if a.id in agent.children ]]
        y = [[agent.pos[2]]; [ a.pos[2] for a in agents if a.id in agent.children ]]
        plot!(x,y, linewidth=3, color=:magenta)
    end
    title!("World ($age)")
    
    # Sugar distribution
    sugar_hist = histogram([ agent.sugar for agent in agents ], bins=( length(agents) > 0 ? range(min([ agent.sugar for agent in agents ]...), max([ agent.sugar for agent in agents ]...), length=10) : 10), label="pop: $(length(agents))", legend=:topright)
    lorenz([ agent.sugar for agent in agents ]) |> v -> plot!(v[1]*(length(agents) > 0 ? max([ agent.sugar for agent in agents ]...) : 1.0), v[2]*(sugar_hist.subplots[1].attr[:yaxis].plotattributes[:extrema].emax != Inf ? sugar_hist.subplots[1].attr[:yaxis].plotattributes[:extrema].emax : 1.0), label="Lorenz", linewidth=lw)
    title!("Sugar dist.")

    # Sugar mean and median
    sugar_plot = plot(xr, history[:sugar_mean][xr], label="mean", linewidth=lw, legend=:topleft)
    plot!(xr, history[:sugar_median][xr], label="median", linewidth=lw)
    plot!(xr, history[:sugar_min][xr], label="min", linewidth=lw)
    plot!(xr, history[:sugar_max][xr], label="max", linewidth=lw)
    title!("Sugar")

    # Sugar Gini
    gini_plot = plot(xr, history[:wealth_gini][xr], label="wealth", linewidth=lw, legend=:topleft)
    plot!(xr, history[:income_gini][xr], label="income", linewidth=lw)
    title!("Gini")
    ylims!((0.0,1.0))

    # Population
    pop_plot = plot(xr, history[:population], label="", linewidth=lw)
    title!("Population")

    # Age
    age_plot = plot(xr, history[:age_mean][xr], label="mean", linewidth=lw, legend=:topleft)
    plot!(xr, history[:age_median][xr], label="median", linewidth=lw)
    plot!(xr, history[:age_min][xr], label="min", linewidth=lw)
    plot!(xr, history[:age_max][xr], label="max", linewidth=lw)
    title!("Age")

    # Death
    death_plot = plot(xr, running_mean(history[:pop_starve][xr]), label="starved", linewidth=lw)
    plot!(xr, running_mean(history[:pop_age][xr]), label="old age", linewidth=lw)
    title!("Cause of death")

    # Vision
    vision_plot = plot(xr, history[:vision_mean][xr], label="mean", linewidth=lw, legend=:topleft)
    plot!(xr, history[:vision_median][xr], label="median", linewidth=lw)
    plot!(xr, history[:vision_min][xr], label="min", linewidth=lw)
    plot!(xr, history[:vision_max][xr], label="max", linewidth=lw)
    title!("Vision")

    # Metabolism
    metabolism_plot = plot(xr, history[:metabolism_mean][xr], label="mean", linewidth=lw, legend=:topleft)
    plot!(xr, history[:metabolism_median][xr], label="median", linewidth=lw)
    plot!(xr, history[:metabolism_min][xr], label="min", linewidth=lw)
    plot!(xr, history[:metabolism_max][xr], label="max", linewidth=lw)
    title!("Metabolism")

    # Fertility
    fertility_plot = plot(xr, history[:fertility_age_ratio][xr], label="age", linewidth=lw)
    plot!(xr, history[:fertility_sugar_ratio][xr], label="sugar", linewidth=lw)
    plot!(xr, history[:fertility_sugar_ratio][xr] .* history[:fertility_age_ratio][xr], label="total", linewidth=lw)
    title!("Fertility")
    ylims!((0.0,1.0))

    gui(plot(
        world_plot,
        #sugar_hist, sugar_plot,
        sugar_hist, sugar_plot, gini_plot,
        pop_plot, age_plot, death_plot,
        vision_plot, metabolism_plot, fertility_plot,
        layout=(@layout [ a{0.5w} [ b e h; c f i; d g j] ]), size=(1280+256, 720)))
end

function world_run()
    t = @elapsed begin

    config = Dict(
        # Global
            # world-shape
        :shape => (50,50),

        # Agents
            # Number of agents
        :agents => 250,
            # Center of agent placement; Vector{Int} or `nothing` (for random)              
        :agent_pos => nothing,
            # Function to generate starting sugar amount
        :agent_sugar => () -> rand()*50+50,
            # Function to generate starting vision
            # "In what follows vision is initially distributed uniformly across agents with values ranging from 1 to 6, unless stated otherwise." (p. 24)
            # "We now turn to a different kind of emergent structure, this one is spatial in nature. To 'grow' it we need to give the agents a maximum vision of ten [...]" (p. 42)
        :agent_vision => () -> rand(2:5),
            # Function to generate agent metabolism
            # "[...] metabolism is uniformly distributed with a minimum of 1 and a maximum of 4." (p. 24)
        :agent_metabolism => () -> rand()*3+1,
            # Function to generate agent starting age
        :agent_age => () -> 0,
            # Function to generate agent life span
        :agent_life_span => () -> rand(60:100),
            # Function to generate fetile age
        :agent_fertile_range => (sex) -> (rand(12:15), sex == :f ? rand(40:50) : rand(50:60)),

        # Landscape
        ## sugar regrowth
            # Distribution for regrowth:
            #   :rate   - constant rate over whole landscape (default)
            #   :random - sum of randomly placed normals
            #   :fixed  - sum of by hand placed normals (see also :regrowth_positions)
        :regrowth_dist => :rate,
            # Regrowth positions; Vector{Int} or nothing (for two diagonally opposed maxima)
            # only if :regrowth_dist is :fixed.
        :regrowth_positions => nothing,
            # Regrowth rate factor
        :regrowth_rate => 1,
            # Rule to use for regrowth, options are
            #   :step - Step all positions :regrowth_rate points every tick (default)
            #   :ns   - North/south seasonal switch (see TODO for season length)
            #   :nnm  - Nearest neighbour mean, growth rate determined by mean sugar
            #           in the Von Neumann neighbourhood
            #   :inf  - Grow to capacity every tick
        :regrowth_rule => :step,
        ## pollution
            # Age at which pollution starts
            # Setting this larger than :plot_frames * :iteration_points disables pollution
        :pollution_start => 20,
            # Age at which pollution diffusion starts
        :pollution_diffusion_start => 40,
            # Pollution created by amount consumed
        :consumption_pollution_coeff => 0.1,
            # Pollution created by amount harvested
        :harvest_pollution_coeff => 0.1,
            # The time intervals between a pollusion diffusion
        :diffusion_interval => 1,

        # Simulation & plots
            # How many frames that should be plotted during the simulation
        :plot_frames => 20,
            # How many simulation iterations per plot frame
            # Total number of simulation points (final world age) is :plot_frames * :iteration_points
            # Real-time simulation corresponds to :iteration_points = 1, to reach
            # high final world age in reasonable time, increase :iteration_points
            # with fixed :plot_frames
        :iteration_points => 50,
            # Int to limit the x-axis to the last number of points, or :all for all.
        :plot_limit => :all,
    )

    # Change defult configuration
    config[:shape] = (50,50)
    config[:agents] = 50

    #config[:agent_pos] = [round.(Int,(config[:shape] ./ 4 .* (1,3)))...]

    config[:regrowth_dist] = :fixed
    #config[:regrowth_positions] = [[round(Int, 3*config[:shape][1]/4), round(Int, 3*config[:shape][2]/4)], [round(Int, config[:shape][1]/4), round(Int, config[:shape][2]/4)], [round(Int, config[:shape][1]/4), round(Int, 3*config[:shape][2]/4)], [round(Int, 3*config[:shape][1]/4), round(Int, config[:shape][2]/4)]]
    #config[:regrowth_rule] = :ns

    config[:pollution_start] = 200000
    config[:pollution_diffusion_start] = 0

    config[:plot_frames] = 1
    config[:iteration_points] = 3000

    xs = [x for x in 0:(config[:shape][1]-1)]
    ys = [y for y in 0:(config[:shape][2]-1)]

    agents = random_agents(config, config[:agent_pos]; shape=config[:shape], length=config[:agents])
    landscape = Landscape(agents, config[:regrowth_dist], shape=config[:shape], rate=config[:regrowth_rate], positions=config[:regrowth_positions])

    history = Dict{Symbol,Vector{Float64}}(
        :sugar_mean => [], :sugar_median => [], :sugar_min => [], :sugar_max => [],
        :wealth_gini => [], :income_gini => [],
        :lives => [],
        :age_mean => [], :age_median => [], :age_min => [], :age_max => [],
        :vision_mean => [], :vision_median => [], :vision_min => [], :vision_max => [],
        :metabolism_mean => [], :metabolism_median => [], :metabolism_min => [], :metabolism_max => [],
        :population => [], :pop_starve => [], :pop_age => [],
        :fertility_age_ratio => [], :fertility_sugar_ratio => []
        )

    age = 0

    plot_frames = config[:plot_frames]
    for _ in 1:plot_frames
        compute_frames = config[:iteration_points]
        for i in 1:compute_frames
            length(agents) == 0 && break

            update_history!(history, agents)

            world_tick!(landscape, agents, age, config, history)
            age += 1

            if i != compute_frames
                for k in keys(landscape.network)
                    delete!(landscape.network, k)
                end
            end
        end
        plot_world(landscape, agents, history, xs, ys, age)
        for k in keys(landscape.network)
            delete!(landscape.network, k)
        end
        length(agents) == 0 && break
    end
    end
    println("time: $t")
    print("'r' to restart: ")
    return readline()
end

function main()

    reply = "r"
    while reply == "r"
        reply = world_run()
    end

end

main()



