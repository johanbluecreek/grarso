# GRowing ARtificial SOcieties -- grarso.jl

A Julia implementation of the simulation software described in "Growing Artificial Societies: Social Science From The Bottom Up", by Joshua M. Epstein, Robert Axtell, https://doi.org/10.7551/mitpress/3374.001.0001.

## Usage

```
$ ./grarso.jl
```

To start a currently configured simulation.

For development purposes, recommended is to

```
$ julia
julia> using Revise
julia> includet("./grarso.jl")
```

after which `main()` can be executed at will, even after changes.